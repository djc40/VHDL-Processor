module ALU(data_operandA, data_operandB, ctrl_ALUopcode, ctrl_shiftamt, data_result, isNotEqual, isLessThan);
   input [31:0] data_operandA, data_operandB;
   input [4:0] ctrl_ALUopcode, ctrl_shiftamt;
   
   wire [31:0] addout,orout,sllout,sraout,invertB,Bin,andout;
   wire cin;
   
   output [31:0] data_result;
   output isNotEqual, isLessThan;
   
   subbit s(ctrl_ALUopcode,cin);
   invert i(data_operandB,invertB);
   submux sm(data_operandB,invertB,cin,Bin);
   ander andy(data_operandA,data_operandB,andout);
   orer orey(data_operandA,data_operandB,orout);
   sll sl(data_operandA,sllout,ctrl_shiftamt);
   sra sr(data_operandA,sraout,ctrl_shiftamt);
   
   compare c(data_operandA,data_operandB,data_result,isNotEqual,isLessThan,cin);
   
   cla add(data_operandA,Bin,cin,addout);
   
   alupicker pick(addout,addout,andout,orout,sllout,sraout,ctrl_ALUopcode,data_result);
      
   endmodule
   
  