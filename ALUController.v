module ALUController(opcode,aluop,rd,alucode,binctrl);

input [4:0] opcode, aluop, rd;
output [4:0] alucode;
output binctrl;
wire [31:0] choice, choice2;



decoder de(opcode,choice);
decoder de2(rd,choice2);
//TODO: put in other options for different opcodes


//Passes the aluop codes(5 digits) to the alu based on the opcode(31:27)
small_tri(aluop,choice[0],alucode); //If op code 0, R-type so only need to send aluop
small_tri(5'b00000,choice[5],alucode); //addi so send add
small_tri(5'b00000,choice[7],alucode); //sw, so send add
small_tri(5'b00000,choice[8],alucode); //lw, so send add
small_tri(5'b00001,choice[2],alucode); //bne, so send sub so that you get the compare vals
small_tri(5'b00001,choice[6],alucode); //blt, so send sub so that you get the compare vals
small_tri(5'b00000,choice[1],alucode); //j, doesn't matter
small_tri(5'b00000,choice[3],alucode); //jal, doesn't matter
small_tri(5'b00000,choice[4],alucode); //jr, doesn't matter

//For ALU input B; Either DLatchB or Sign extended
one_tri(1'b1,choice[0] & ~choice2[22] & ~choice2[23] & ~choice2[24] & ~choice2[25]  ,binctrl); //If op code 0, R-Type, need DLatchB
one_tri(1'b0,choice[0] & (choice2[22] | choice2[23] | choice2[24] | choice2[25])  ,binctrl); //If opcode 0, and input, need N
one_tri(1'b0,choice[5],binctrl); //addi, need N
one_tri(1'b0,choice[7],binctrl); //sw, need N
one_tri(1'b0,choice[8],binctrl); //lw, need N
one_tri(1'b1,choice[2],binctrl); //bne, so need dlatchB for Rd
one_tri(1'b1,choice[6],binctrl); //blt, so need dlatchB for Rd
one_tri(1'b1,choice[1],binctrl); //j, shouldn't matter
one_tri(1'b1,choice[3],binctrl); //jal, shouldn't matter
one_tri(1'b1,choice[4],binctrl); //jr, shouldn't matter





endmodule