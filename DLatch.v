module DLatcha(clock,pcin,Ain,Bin,irin,pcout,Aout,Bout,irout,reset);
input clock,reset;
input [31:0] pcin,Ain,Bin,irin;
output [31:0] pcout,Aout,Bout,irout;

my_dffdlatch pc(pcin, reset, clock, 1, pcout);
my_dffdlatch a(Ain, reset, clock, 1, Aout);
my_dffdlatch b(Bin, reset, clock, 1, Bout);
my_dffdlatch ir(irin, reset, clock, 1, irout);


endmodule 