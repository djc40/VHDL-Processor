module FLatch(pcin,irin,clock, pcout, irout,en, reset);
input clock,en,reset;
input [31:0] pcin, irin;
output [31:0] pcout, irout;

my_dffdlatch PC(pcin, reset, clock, en, pcout);

my_dffdlatch IR(irin, reset, clock, en, irout);

endmodule