module vga(FPGA_clock, vga_clock, R, G, B, vga_hs_sync, vga_vs_sync, vga_blank, vga_sync, plY, prY, bX, bY);//, oneup, onedown, twoup, twodown);//, LED_vga_hs_ON, LED_vga_vs_on);
	input FPGA_clock;//, oneup, onedown, twoup, twodown;
	output vga_clock, vga_hs_sync, vga_vs_sync, vga_blank, vga_sync;
	output[7:0] R, G, B;
	input [31: 0] plY, prY, bX, bY; 
	
	assign vga_hs_sync = ~vga_hs;
	assign vga_vs_sync = ~vga_vs;
	assign vga_blank = vga_hs_sync;
	
	
	//640x480
	//counterX counts 0 - 767, generates HS
	reg[9:0] counterX; 
	//counterY counts 0 to 511, generates VS
	reg[8:0] counterY; 
	wire counterXmaxed;

	reg vga_hs, vga_vs;
	assign vga_sync = 1'b0;
		
	//Clock
	 pllVGA clockDivide(FPGA_clock, vga_clock); //Divide FPGA clock by 2, 50MHz/2 = 25MHz
	
	//Draw objects
	wire border		   =   (counterX[9:3]==16)          || (counterX[9:3]==92)           || (counterY[8:3]==2)            || (counterY[8:3]==63);
	wire ball         = (((counterX[9:0] > (170 + bX)) && (counterX[9:0] < (195 + bX))) && ((counterY[8:0] > (25 + bY))  && (counterY[8:0] < (50  + bY))));
	wire paddle_left  = (((counterX[9:0] > 150)        && (counterX[9:0] < 170))        && ((counterY[8:0] > (20 + plY)) && (counterY[8:0] < (120 + plY))));
	wire paddle_right = (((counterX[9:0] > 700)        && (counterX[9:0] < 720))        && ((counterY[8:0] > (20 + prY)) && (counterY[8:0] < (120 + prY))));

	//Color objects
	assign R[7] = border || paddle_left; 
	assign G[7] = border || ball; 
	assign B[7] = border || paddle_right; 

	//HS and VS counters
	assign counterXmaxed = (counterX == 767);
	
	always @(posedge vga_clock) begin
		if(counterXmaxed) 
			begin
				counterX = 0;
				counterY = counterY + 9'b000000001; 
			end
		else
			counterX = counterX + 10'b0000000001; 
	end

 //HS and VS clock signals
	always @(posedge vga_clock) begin
		vga_hs = (counterX[9:4] == 0); //16 clocks
		vga_vs = (counterY==0); //768 clocks
	end
endmodule