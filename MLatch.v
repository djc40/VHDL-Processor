module MLatch(clock, O,D,IR, Oout, Dout, IRout,reset);
input clock,reset;
input [31:0] O, D, IR;
output [31:0] Oout, Dout, IRout;


my_dffdlatch dffO(O, reset, clock, 1, Oout);
my_dffdlatch dffD(D, reset, clock, 1, Dout);
my_dffdlatch dffIR(IR, reset, clock,1, IRout);

endmodule 