module PC(d, aclr, clk, enable, f);

input aclr, clk, enable;
input [31:0] d;
 output [31:0] f; 
 reg [31:0] f;
always @(posedge clk or posedge aclr) begin
 if(aclr) begin
 f = 1'b0;
 
end else begin 
if(enable) begin
f = d;

end
end
end
endmodule 