module XLatch(clock,O,B,IR,Oout,Bout,irout,reset);

input clock,reset;
input [31:0] O, B,IR;
output [31:0] Oout,Bout,irout;
my_dffdlatch dff0(O, reset, clock, 1, Oout);
my_dffdlatch dff1(B, reset, clock, 1, Bout);
my_dffdlatch dff2(IR, reset, clock, 1, irout);

endmodule 