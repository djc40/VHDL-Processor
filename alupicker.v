module alupicker(d0,d1,d2,d3,d4,d5,code, q);
input [31:0] d0,d1,d2,d3,d4,d5;
input [4:0] code;
output [31:0] q;

wire [31:0] choice;

decoder de(code, choice);

bigmy_tri tri00(d0,choice[0],q);
bigmy_tri tri01(d1,choice[1],q);
bigmy_tri tri02(d2,choice[2],q);
bigmy_tri tri03(d3,choice[3],q);
bigmy_tri tri04(d4,choice[4],q);
bigmy_tri tri05(d5,choice[5],q);


endmodule