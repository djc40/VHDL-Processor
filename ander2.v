module ander2(x,y,q);

input x;
input [31:0] y;
output [31:0] q;

generate
genvar i;
for(i = 0; i<32; i = i+1)
begin: gen1
assign q[i] = x&y[i];
end
endgenerate

endmodule