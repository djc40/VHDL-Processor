module branchDetector(Dir,lt,ne,isbranch);
input [31:0] Dir;
input lt,ne; //lt is 1 when Rs < Rd, ne is 1 when Rs ~= Rd
output isbranch;

wire [4:0] Dop;
wire A,B;

assign Dop = Dir[31:27];

assign A = (Dop[0] ~^ 1'b0) & (Dop[1] ~^ 1'b1) & (Dop[2] ~^ 1'b0) & (Dop[3] ~^ 1'b0) & (Dop[4] ~^ 1'b0);//If BNE
assign B = (Dop[0] ~^ 1'b0) & (Dop[1] ~^ 1'b1) & (Dop[2] ~^ 1'b1) & (Dop[3] ~^ 1'b0) & (Dop[4] ~^ 1'b0);//IF Blt

assign isbranch = (A & ne) | (B & ~lt & ne);

endmodule