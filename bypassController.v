module bypassController(Dir,Xir,Mir,Amux,Bmux,memMux);
input [31:0] Dir, Xir, Mir;
output [1:0] Amux, Bmux;
output memMux;

wire [4:0] Drs,Drd,Drt,Xrd,Mrd,Dpc,Xop,Mop;
wire conA,conB,conC,conD,conE,conF,conG, conH, conI, conJ, conK;
//A mux logic
assign Dpc = Dir[31:27];
assign Drd = Dir[26:22];
assign Drt = Dir[16:12];
assign Drs = Dir[21:17];
assign Xrd = Xir[26:22];
assign Mrd = Mir[26:22];
assign Xop = Xir[31:27];
assign Mop = Mir[31:27];

//TODO: Finish fixing bypass logic for lw


assign conJ = ((Xop[0] ~^ 1'b1) & (Xop[1] ~^ 1'b1) & (Xop[2] ~^ 1'b1) & (Xop[3] ~^ 1'b0) & (Xop[4] ~^ 1'b0)); //Xop = sw, don't bypass back; 
assign conK = ((Mop[0] ~^ 1'b1) & (Mop[1] ~^ 1'b1) & (Mop[2] ~^ 1'b1) & (Mop[3] ~^ 1'b0) & (Mop[4] ~^ 1'b0)); //Mop = sw, don't bypass back; 
assign conL = ((Xop[0] ~^ 1'b0) & (Xop[1] ~^ 1'b0) & (Xop[2] ~^ 1'b0) & (Xop[3] ~^ 1'b1) & (Xop[4] ~^ 1'b0)); //Xop = lw, don't bypass back; 
assign conM = ((Xop[0] ~^ 1'b0) & (Xop[1] ~^ 1'b1) & (Xop[2] ~^ 1'b0) & (Xop[3] ~^ 1'b0) & (Xop[4] ~^ 1'b0)); //Xop = bne, don't bypass back;
assign conN = ((Mop[0] ~^ 1'b0) & (Mop[1] ~^ 1'b1) & (Mop[2] ~^ 1'b0) & (Mop[3] ~^ 1'b0) & (Mop[4] ~^ 1'b0)); //Mop = bne, don't bypass back; 




assign conA = (Drs[0] ~^ Xrd[0]) & (Drs[1] ~^ Xrd[1]) & (Drs[2] ~^ Xrd[2]) & (Drs[3] ~^ Xrd[3]) & (Drs[4] ~^ Xrd[4]); //D.rs == X.rd
assign conB = (Drs[0] ~^ Mrd[0]) & (Drs[1] ~^ Mrd[1]) & (Drs[2] ~^ Mrd[2]) & (Drs[3] ~^ Mrd[3]) & (Drs[4] ~^ Mrd[4]); //D.rs = M.rd
 
two_tri(2'b00,~conA & ~conB | ((conJ | conL | conM) & conA) | ((conK|conN) & conB),Amux); //0 dlatchA
two_tri(2'b01,~conA & conB & ~conK & ~conN,Amux);				//1 regwritedata
two_tri(2'b10,conA & ~conJ & ~conL & ~conM ,Amux);			//2 xlatchO

//B mux logic
assign conC = (Drt[0] ~^ Xrd[0]) & (Drt[1] ~^ Xrd[1]) & (Drt[2] ~^ Xrd[2]) & (Drt[3] ~^ Xrd[3]) & (Drt[4] ~^ Xrd[4]); //D.rt == X.rd
assign conD = (Drt[0] ~^ Mrd[0]) & (Drt[1] ~^ Mrd[1]) & (Drt[2] ~^ Mrd[2]) & (Drt[3] ~^ Mrd[3]) & (Drt[4] ~^ Mrd[4]); //D.rt == M.rd
assign conE = ((Dpc[0] ~^ 1'b0) & (Dpc[1] ~^ 1'b1) & (Dpc[2] ~^ 1'b0) & (Dpc[3] ~^ 1'b0) & (Dpc[4] ~^ 1'b0)) | ((Dpc[0] ~^ 1'b0) & (Dpc[1] ~^ 1'b1) & (Dpc[2] ~^ 1'b1) & (Dpc[3] ~^ 1'b0) & (Dpc[4] ~^ 1'b0)) | ((Dpc[0] ~^ 1'b1) & (Dpc[1] ~^ 1'b1) & (Dpc[2] ~^ 1'b1) & (Dpc[3] ~^ 1'b0) & (Dpc[4] ~^ 1'b0)) | ((Dpc[0] ~^ 1'b0) & (Dpc[1] ~^ 1'b0) & (Dpc[2] ~^ 1'b1) & (Dpc[3] ~^ 1'b0) & (Dpc[4] ~^ 1'b0));
	//If bne, blt, jr, or sw need to bypass rd;
assign conF = (Drd[0] ~^ Xrd[0]) & (Drd[1] ~^ Xrd[1]) & (Drd[2] ~^ Xrd[2]) & (Drd[3] ~^ Xrd[3]) & (Drd[4] ~^ Xrd[4]); //D.rd == X.rd for bne, blt, or sw
assign conG = (Drd[0] ~^ Mrd[0]) & (Drd[1] ~^ Mrd[1]) & (Drd[2] ~^ Mrd[2]) & (Drd[3] ~^ Mrd[3]) & (Drd[4] ~^ Mrd[4]); //D.rd == M.rd for bne, blt, or sw

assign conH = ((Dpc[0] ~^ 1'b0) & (Dpc[1] ~^ 1'b0) & (Dpc[2] ~^ 1'b0) & (Dpc[3] ~^ 1'b0) & (Dpc[4] ~^ 1'b0)) | ((Dpc[0] ~^ 1'b1) & (Dpc[1] ~^ 1'b0) & (Dpc[2] ~^ 1'b1) & (Dpc[3] ~^ 1'b0) & (Dpc[4] ~^ 1'b10));


two_tri(2'b00,(conH & ~conC & ~conD)|(conE & ~conF & ~conG)|(~conH & ~conE)|((conJ | conL | conM) & conC) | ((conJ | conL | conM) & conF) | ((conK | conN) & conD) | ((conK | conN) & conG), Bmux); //0 dlatchA
two_tri(2'b01,((conH & conD & ~conC) | (conE & conG & ~conF)) & ~conK & ~conN ,Bmux);				//1 regwritedata
two_tri(2'b10, ((conH & conC) | (conE & conF)) & ~conJ & ~conL & ~conM,Bmux);			//2 xlatchO


//Mem mux logic

assign conI = (Xrd[0] ~^ Mrd[0]) & (Xrd[1] ~^ Mrd[1]) & (Xrd[2] ~^ Mrd[2]) & (Xrd[3] ~^ Mrd[3]) & (Xrd[4] ~^ Mrd[4]);
one_tri(1'b0,~conI | ~conJ | conK | conM,memMux);// 0 xlatch b
one_tri(1'b1,conI & conJ & ~conK & ~conN,memMux);// 1 regwritedata




endmodule