module compare(A,B,S,Eq,Lt,Cin);

input [31:0] A,B,S;
input Cin;
output Eq,Lt;


assign Eq = Cin&(S[0]|S[1]|S[2]|S[3]|S[4]|S[5]|S[6]|S[7]|S[8]|S[9]|S[10]|S[11]|S[10]|S[11]|S[12]|S[13]|S[14]|S[15]|S[16]|S[17]|S[18]|S[19]|S[20]|S[21]|S[22]|S[23]|S[24]|S[25]|S[26]|S[27]|S[28]|S[29]|S[30]|S[31]);
assign Lt = Cin&((~A[31]&~B[31]&S[31]) | (A[31]&~B[31]) | (A[31]&B[31]&S[31]));
endmodule