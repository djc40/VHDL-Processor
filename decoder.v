module decoder(d,q);
	input [4:0] d;
	output [31:0] q;
	
	assign q[0] = ~d[0] & ~d[1] & ~d[2] & ~d[3] & ~d[4];
	assign q[1] = d[0] & ~d[1] & ~d[2] & ~d[3] & ~d[4];
	assign q[2] = ~d[0] & d[1] & ~d[2] & ~d[3] & ~d[4];
	assign q[3] = d[0] & d[1] & ~d[2] & ~d[3] & ~d[4];
	assign q[4] = ~d[0] & ~d[1] & d[2] & ~d[3] & ~d[4];
	assign q[5] = d[0] & ~d[1] & d[2] & ~d[3] & ~d[4];
	assign q[6] = ~d[0] & d[1] & d[2] & ~d[3] & ~d[4];
	assign q[7] = d[0] & d[1] & d[2] & ~d[3] & ~d[4];
	assign q[8] = ~d[0] & ~d[1] & ~d[2] & d[3] & ~d[4];
	assign q[9] = d[0] & ~d[1] & ~d[2] & d[3] & ~d[4];
	assign q[10] = ~d[0] & d[1] & ~d[2] & d[3] & ~d[4];
	assign q[11] = d[0] & d[1] & ~d[2] & d[3] & ~d[4];
	assign q[12] = ~d[0] & ~d[1] & d[2] & d[3] & ~d[4];
	assign q[13] = d[0] & ~d[1] & d[2] & d[3] & ~d[4];
	assign q[14] = ~d[0] & d[1] & d[2] & d[3] & ~d[4];
	assign q[15] = d[0] & d[1] & d[2] & d[3] & ~d[4];
	assign q[16] = ~d[0] & ~d[1] & ~d[2] & ~d[3] & d[4];
	assign q[17] = d[0] & ~d[1] & ~d[2] & ~d[3] & d[4];
	assign q[18] = ~d[0] & d[1] & ~d[2] & ~d[3] & d[4];
	assign q[19] = d[0] & d[1] & ~d[2] & ~d[3] & d[4];
	assign q[20] = ~d[0] & ~d[1] & d[2] & ~d[3] & d[4];
	assign q[21] = d[0] & ~d[1] & d[2] & ~d[3] & d[4];
	assign q[22] = ~d[0] & d[1] & d[2] & ~d[3] & d[4];
	assign q[23] = d[0] & d[1] & d[2] & ~d[3] & d[4];
	assign q[24] = ~d[0] & ~d[1] & ~d[2] & d[3] & d[4];
	assign q[25] = d[0] & ~d[1] & ~d[2] & d[3] & d[4];
	assign q[26] = ~d[0] & d[1] & ~d[2] & d[3] & d[4];
	assign q[27] = d[0] & d[1] & ~d[2] & d[3] & d[4];
	assign q[28] = ~d[0] & ~d[1] & d[2] & d[3] & d[4];
	assign q[29] = d[0] & ~d[1] & d[2] & d[3] & d[4];
	assign q[30] = ~d[0] & d[1] & d[2] & d[3] & d[4];
	assign q[31] = d[0] & d[1] & d[2] & d[3] & d[4];
	
	
endmodule
