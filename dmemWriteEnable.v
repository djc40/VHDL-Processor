module dmemWriteEnable(opcode,dmemwriteEn);
input [4:0] opcode;
output dmemwriteEn;
wire [31:0] choice;

decoder de(opcode,choice);

//For dmem write enable; either 1 or 0, 1->write, 0->don't write
one_tri(1'b1, choice[7],dmemwriteEn); //If Sw, need 1
one_tri(1'b0, ~choice[7],dmemwriteEn); //If not Sw, need 0

endmodule