module eightbit(x0,x1,x2,x3,x4,x5,x6,x7,y0,y1,y2,y3,y4,y5,y6,y7,cin,cout,s0,s1,s2,s3,s4,s5,s6,s7);

	input x0,x1,x2,x3,x4,x5,x6,x7,y0,y1,y2,y3,y4,y5,y6,y7,cin;
	wire [7:0] G0,P0;
	output s0,s1,s2,s3,s4,s5,s6,s7,cout;
	
	wire c1,p0,g0,c2,p1,g1,c3,p2,g2,c4,p3,g3,c5,p4,g4,c6,p5,g5,c7,p6,g6,c8,p7,g7;

	onebitCLA one(x0,y0,cin,s0,c1,p0,g0);
	onebitCLA two(x1,y1,c1,s1,c2,p1,g1);
	onebitCLA three(x2,y2,c2,s2,c3,p2,g2);
	onebitCLA four(x3,y3,c3,s3,c4,p3,g3);
	onebitCLA five(x4,y4,c4,s4,c5,p4,g4);
	onebitCLA six(x5,y5,c5,s5,c6,p5,g5);
	onebitCLA seven(x6,y6,c6,s6,c7,p6,g6);
	onebitCLA eight(x7,y7,c7,s7,c8,p7,g7);
	
	assign G0 = g7 | p7&g6 | p7&p6&g5 | p7&p6&p5&g4 | p7&p6&p5&p4&g3 | p7&p6&p5&p4&p3&g2 | p7&p6&p5&p4&p3&p2&g1 | p7&p6&p5&p4&p3&p2&p1&g0;
	assign P0 = p7&p6&p5&p4&p3&p2&p1&p0;
	assign cout = G0 | P0&cin;
	
endmodule
