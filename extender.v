module extender(d,f);

input [16:0] d;
output[31:0] f;


generate
genvar i;
for (i=0; i < 17; i = i+1)
begin : gen1
assign f[i] = d[i];
end

endgenerate

generate
genvar j;
for(j=17; j<32; j = j+1)
begin: gen2
assign f[j] = d[11];
end
endgenerate
endmodule