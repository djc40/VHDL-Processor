module fivemux(N, but1, but2, but3, but4, ctrl, out);
input [4:0] ctrl;

input [16:0] N;
input but1,but2,but3,but4;
output [16:0] out;

wire [31:0] choice;
wire [16:0] out1,out2,out3,out4;

decoder de(ctrl,choice);

genvar i;
generate
for(i = 1; i < 17; i = i + 1) begin: gen1
assign out1[i] = 0;
assign out2[i] = 0;
assign out3[i] = 0;
assign out4[i] = 0;
end
endgenerate

assign out1[0] = but1;
assign out2[0] = but2;
assign out3[0] = but3;
assign out4[0] = but4;


// my_tri(in, oe, out)
my_tri(out4,choice[25],out); //on 25 output
my_tri(out3,choice[24],out); //on 24 output
my_tri(out2,choice[23],out); //on 23 output
my_tri(out1,choice[22],out); //on 22 output
my_tri(N,(~choice[22] & ~choice[23] & ~choice[24] & ~choice[25]) ,out); //on when not button input


endmodule