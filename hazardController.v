module hazardController(Fir, Dir, muxnop,Fstall,PCstall);
input [31:0] Fir, Dir;
output muxnop,Fstall,PCstall;

wire [4:0] Dop,Fop,Frs,Frt,Frd,Drd;
wire A,B,C,D,E,F,G,H,I,J,K;
//During stall muxnop = 1, Fstall = 0, PCstall = 0;

assign Dop = Dir[31:27];
assign Fop = Fir[31:27];
assign Frs = Fir[21:17];
assign Frt = Fir[16:12];
assign Frd = Fir[26:22];
assign Drd = Dir[26:22];

assign A = (Dop[0] ~^ 1'b0) & (Dop[1] ~^ 1'b0) & (Dop[2] ~^ 1'b0) & (Dop[3] ~^ 1'b1) & (Dop[4] ~^ 1'b0); //D.op = Load   

assign B = (Fop[0] ~^ 1'b0) & (Fop[1] ~^ 1'b0) & (Fop[2] ~^ 1'b0) & (Fop[3] ~^ 1'b0) & (Fop[4] ~^ 1'b0); //F.op = Rtype
assign C = (Fop[0] ~^ 1'b1) & (Fop[1] ~^ 1'b0) & (Fop[2] ~^ 1'b1) & (Fop[3] ~^ 1'b0) & (Fop[4] ~^ 1'b0); //F.op = addi
assign D = (Fop[0] ~^ 1'b0) & (Fop[1] ~^ 1'b1) & (Fop[2] ~^ 1'b0) & (Fop[3] ~^ 1'b0) & (Fop[4] ~^ 1'b0); //F.op = bne
assign E = (Fop[0] ~^ 1'b0) & (Fop[1] ~^ 1'b1) & (Fop[2] ~^ 1'b1) & (Fop[3] ~^ 1'b0) & (Fop[4] ~^ 1'b0); //F.op = blt
assign F = (Fop[0] ~^ 1'b1) & (Fop[1] ~^ 1'b1) & (Fop[2] ~^ 1'b1) & (Fop[3] ~^ 1'b0) & (Fop[4] ~^ 1'b0); //F.op = sw
assign G = (Fop[0] ~^ 1'b0) & (Fop[1] ~^ 1'b0) & (Fop[2] ~^ 1'b0) & (Fop[3] ~^ 1'b1) & (Fop[4] ~^ 1'b0); //F.op = lw
assign H = (Fop[0] ~^ 1'b0) & (Fop[1] ~^ 1'b0) & (Fop[2] ~^ 1'b1) & (Fop[3] ~^ 1'b0) & (Fop[4] ~^ 1'b0); //F.op = jr

assign I = (Frs[0] ~^ Drd[0]) & (Frs[1] ~^ Drd[1]) & (Frs[2] ~^ Drd[2]) & (Frs[3] ~^ Drd[3]) & (Frs[4] ~^ Drd[4]); //F.rs = D.rd
assign J = (Frt[0] ~^ Drd[0]) & (Frt[1] ~^ Drd[1]) & (Frt[2] ~^ Drd[2]) & (Frt[3] ~^ Drd[3]) & (Frt[4] ~^ Drd[4]); //F.rt = D.rd
assign K = (Frd[0] ~^ Drd[0]) & (Frd[1] ~^ Drd[1]) & (Frd[2] ~^ Drd[2]) & (Frd[3] ~^ Drd[3]) & (Frd[4] ~^ Drd[4]); //F.rd = D.rd




assign stall = (A & (((B|C|D|E|F|G) & I) | ((B & J) | (D & K) | (H & K)| (E & K)) & ~F ));

assign muxnop = stall;
assign Fstall = ~stall;
assign PCstall = ~stall;



endmodule 