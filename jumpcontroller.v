module jumpcontroller(Dir, curPC,N, Rd, PCout, isjump, isjal);
input [31:0] Dir, curPC;
input [26:0] N;
input [31:0] Rd;
output [31:0] PCout;
output isjump, isjal;

wire [4:0] Dop;
wire [31:0] pc1;

wire A, B, C, ctrl;

assign Dop = Dir[31:27];

assign A = (Dop[0] ~^ 1'b1) & (Dop[1] ~^ 1'b0) & (Dop[2] ~^ 1'b0) & (Dop[3] ~^ 1'b0) & (Dop[4] ~^ 1'b0);// Dop = J
assign B = (Dop[0] ~^ 1'b1) & (Dop[1] ~^ 1'b1) & (Dop[2] ~^ 1'b0) & (Dop[3] ~^ 1'b0) & (Dop[4] ~^ 1'b0);// Dop = Jal
assign C = (Dop[0] ~^ 1'b0) & (Dop[1] ~^ 1'b0) & (Dop[2] ~^ 1'b1) & (Dop[3] ~^ 1'b0) & (Dop[4] ~^ 1'b0);// Dop = Jr


assign isjump = A | B | C;
assign isjal = B;

genvar i;
generate
for(i = 0; i < 27; i = i+1) begin: gen1
assign pc1[i] = N[i];
end
endgenerate

genvar j;
generate
for(j = 27; j<32; j = j+1) begin:gen2
assign pc1[j] = curPC[j];
end
endgenerate


my_tri(pc1,A | B, PCout); //on 1 output
my_tri(Rd,C,PCout); //on 1 output



 

endmodule