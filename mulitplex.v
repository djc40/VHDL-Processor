module mulitplex(d0,d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12,d13,d14,d15,d16,d17,d18,d19,d20,d21,d22,d23,d24,d25,d26,d27,d28,d29,d30,d31,code, q);
input [31:0] d0,d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12,d13,d14,d15,d16,d17,d18,d19,d20,d21,d22,d23,d24,d25,d26,d27,d28,d29,d30,d31;
input [4:0] code;
output [31:0] q;

wire [31:0] choice;

decoder de(code, choice);

my_tri tri00(d0,choice[0],q);
my_tri tri01(d1,choice[1],q);
my_tri tri02(d2,choice[2],q);
my_tri tri03(d3,choice[3],q);
my_tri tri04(d4,choice[4],q);
my_tri tri05(d5,choice[5],q);
my_tri tri06(d6,choice[6],q);
my_tri tri07(d7,choice[7],q);
my_tri tri08(d8,choice[8],q);
my_tri tri09(d9,choice[9],q);
my_tri tri10(d10,choice[10],q);
my_tri tri11(d11,choice[11],q);
my_tri tri12(d12,choice[12],q);
my_tri tri13(d13,choice[13],q);
my_tri tri14(d14,choice[14],q);
my_tri tri15(d15,choice[15],q);
my_tri tri16(d16,choice[16],q);
my_tri tri17(d17,choice[17],q);
my_tri tri18(d18,choice[18],q);
my_tri tri19(d19,choice[19],q);
my_tri tri20(d20,choice[20],q);
my_tri tri21(d21,choice[21],q);
my_tri tri22(d22,choice[22],q);
my_tri tri23(d23,choice[23],q);
my_tri tri24(d24,choice[24],q);
my_tri tri25(d25,choice[25],q);
my_tri tri26(d26,choice[26],q);
my_tri tri27(d27,choice[27],q);
my_tri tri28(d28,choice[28],q);
my_tri tri29(d29,choice[29],q);
my_tri tri30(d30,choice[30],q);
my_tri tri31(d31,choice[31],q);


endmodule 