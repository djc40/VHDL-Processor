module multi(in0,in1,out,choice);
	input in0,in1;
	input choice;
	
	output out;
	
	my_tri t0(in0,~choice,out);
	my_tri t1(in1,choice,out);
	endmodule