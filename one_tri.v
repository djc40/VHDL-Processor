module one_tri(in, oe, out);
	input  in;
	input oe;
	
	output out;

	assign out = oe ? in :1'bz;
endmodule