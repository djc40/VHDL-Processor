module onebitCLA(x0,y0,c0,s0,c1,p0,g0);

	input x0,y0,c0;
	output s0,c1,p0,g0;
	
	assign p0 = x0|y0;
	assign g0 = x0&y0;
	assign c1 = g0|(p0&c0);
	assign s0 = ~x0&~y0&c0 | ~x0&y0&~c0 | x0&~y0&~c0 | x0&y0&c0;
	
	
	
	
	
	

endmodule
