module orer(x,y,q);


input [31:0] x,y;
output [31:0] q;

assign q = x|y;

endmodule
