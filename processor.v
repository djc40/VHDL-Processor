module processor(clock, reset, ps2_key_pressed, ps2_out, lcd_write, lcd_data, debug_data, debug_addr,but1,but2,but3,but4,paddle1,paddle2,ballx,bally);

	input 			clock, reset, ps2_key_pressed,but1,but2,but3,but4;
	input 	[7:0]	ps2_out;
	
	output 			lcd_write;
	output [31:0] 	 	lcd_data,paddle1,paddle2,ballx,bally;
	
	//Debuggers
	//Latch

	//Register

	
	
	// GRADER OUTPUTS - YOU MUST CONNECT TO YOUR DMEM
	output 	[31:0] 	debug_data;
	output	[11:0]	debug_addr;
	
	//PC wires
	wire [31:0] pcout;
	wire [31:0] isNotEqual, isLessThan, aluout,aluin;
	
	//Fetch wires	
	wire [31:0] imemout, FlatchPC, FlatchIR, regoutA, regoutB, writedata;
	wire [4:0] writedest;
	
	//Register wires
	wire [4:0] Rd, Rs, Rt, Bin;
	//Decode wires
	wire [31:0] DlatchPC, DlatchIR, DlatchA, DlatchB,pcval,FirIn,nop1stall;
	
	
	// your processor here
	//
	
	//PC
	
	//TODO: clear logic
	PC mypc(pcval, reset, clock, PCstall, pcout);
	
	
	ALU myalu(pcout, 32'b00000000000000000000000000000001, 5'b00000, 5'b00000, aluout, ,);
	//*PC
	//TODO: ctrl
	//threemuxpc(in1, in2, in3, isbranch,isjump, out);
	threemuxpc(aluout, Jpc, branchpc, isbranch,isjump, pcval); //0-> aluout, 1->jump, 2-> branch
	
	
	assign nop1stall = isbranch | isjump;
	//FLatch
	
	twomux nop1(32'h00000000,imemout,nop1stall,FirIn); // 1-> nop, 0-> imemout
	
	FLatch latch0(pcout,FirIn,clock, FlatchPC, FlatchIR,Fstall,reset);
	
	hazardController hazardtrack(FlatchIR, DlatchIR, NOPstall,Fstall,PCstall);

	
	
	assign Rd = DirIn[26:22];
	assign Rs = DirIn[21:17];
	assign Rt = DirIn[16:12];

	//*FLatch
	

	
	
	wire  wEnable, nop2stall;
	
	//Register
	//module regfile(clock, ctrl_writeEnable, ctrl_reset, ctrl_writeReg, ctrl_readRegA, ctrl_readRegB, data_writeReg, data_readRegA, data_readRegB);
	
	regWriteEnable(IRout[31:27], regwriteEn, regwritectrl);
	
	regfile myreg(clock, regwriteEn, reset, IRout[26:22], Rs, regs2in, regwritedata, regoutA, regoutB,paddle1,paddle2,ballx,bally); //writedest will be the RD from output after data memory and write data comes from after mem stage
	
	//assign debug_addr = IRout[26:22];
	//assign debug_data = regwritedata;
	//assign lcd_write = regwriteEn;


	//*Register
	
	//DLatch
	//DLatcha(clock,pcin,Ain,Bin,irin,pcout,Aout,Bout,irout)
	
	assign nop2stall = NOPstall | isbranch | isjump;
	twomux nop2(32'h00000000,FlatchIR,nop2stall,DirIn);
	DLatcha latch1(clock,FlatchPC,regoutA,regoutB,DirIn,DlatchPC,DlatchA,DlatchB,DlatchIR,reset);


	//*DLatch
	
	//Execute
	wire nE,lT,isjump,isjal;
	wire [31:0] sxi, newpc, muxB, result,muxbit,DirIn,Jpc;
	wire [16:0] extin;
	wire [4:0] alucode;
	//sign extender
	
	//fivemux(N, but1, but2, but3, but4, ctrl, out);
	
	fivemux inputer(DlatchIR[16:0],~but1,~but2,~but3,~but4,DlatchIR[16:12],extin);
	extender extend(extin,sxi);
	//assign lcd_data = sxi;
	
	//jumpcontroller(Dir, curPC,N, Rd, PCout, isjump, isjal);
	jumpcontroller(DlatchIR, DlatchPC,DlatchIR[26:0],bypass2, Jpc, isjump, isjal);
	
	branchDetector(DlatchIR,lT,nE,isbranch);

	
	wire alubctrl,regctrl,regwritectrl,regwriteEn,dmemwriteEn;
	
	//module ALUController(opcode,aluop,alucode,binctrl, reginctrl,regwritectrl,dmemwrite);
	ALUController(DlatchIR[31:27],DlatchIR[6:2],DlatchIR[16:12],alucode,alubctrl); 
	
	

	
	
	wire memMux;
	wire [1:0] Amux, Bmux;
	bypassController Bcontroller(DlatchIR,XlatchIR,IRout,Amux,Bmux,memMux);
	
	
		
	wire [31:0] bypass1,bypass2;
	
	threemux bypassALUinA(DlatchA, regwritedata, XlatchO, Amux, bypass1); //ALUinA bypass Mux; 0->dlatchA, 1->regwritedata, 2->xlatcho
	
	ALU mainALU(bypass1, aluinb, alucode, DlatchIR[11:7], result, nE, lT);
	//*Execute
	
	
	//Branch ALU
	//ALU(data_operandA, data_operandB, ctrl_ALUopcode, ctrl_shiftamt, data_result, isNotEqual, isLessThan);
	ALU brancher(DlatchPC, sxi, 5'b00000, 5'b00000, branchpc,,);
	
	
	
	//XLatch
	wire [31:0] XlatchO, XlatchB, XlatchIR, branchpc;
	//XLatch(clock,O,B,IR,Oout,Bout,irout)
	
	twomux jumpin(DlatchPC,result,isjal,XinO); //0->result, 1->FlatchPC
	
	twomux jumpIR(32'b00011111110000000000000000000000,DlatchIR,isjal,XinIR);
	
	XLatch latch2(clock,XinO,bypass2,XinIR,XlatchO,XlatchB,XlatchIR,reset);
	

	//*XLatch
	
	//MLatch
	wire [31:0] dmemout, Oout, Dout, IRout,aluinb,regs2in,regwritedata,dmemdata,XinO,XinIR;
	wire Fstall,PCstall,NOPstall;
	MLatch latch3(clock, XlatchO,dmemout,XlatchIR, Oout, Dout, IRout,reset);

	
	
	regReadIn(DirIn[31:27],regctrl);
	twomuxfive regs2(Rt, Rd, regctrl, regs2in); //Register input s2; 1->Rt, 0->Rd
	
	
	//TODO: Put in multiplier and divider
	//TODO: Put in reset
	//TODO: Bex
	//TODO: Setx

		
	twomux bypassMEM(regwritedata,XlatchB, memMux, dmemdata); //Mem bypass; 0->xlatchB, 1->regwrite
	
	threemux bypassALUinB(DlatchB, regwritedata, XlatchO, Bmux, bypass2); //ALUinB bypass Mux; 0->DlatchB, 1->bypass regwritedata, 2->bypass xlatchO
	
	twomux aluB(bypass2,sxi,alubctrl,aluinb); //alu input B; 1 -> bypass mux, 0 -> Sign extend
	
	twomux regwrite(Oout, Dout, regwritectrl, regwritedata);  //register write data; 1-> O, 0->D
	
	


	//*MLatch
	
///	one_tri muxer8(0,muxbit[8],en);
	//one_tri muxer9(1,muxbit[9],en);
	
	

	
	dmemWriteEnable(XlatchIR[31:27],dmemwriteEn);//Decides based on Xir opcode whether to write enable dmem
	
	//////////////////////////////////////
	////// THIS IS REQUIRED FOR GRADING
	// CHANGE THIS TO ASSIGN YOUR DMEM WRITE ADDRESS ALSO TO debug_addr
	assign debug_addr = (XlatchO[11:0]);
	// CHANGE THIS TO ASSIGN YOUR DMEM DATA INPUT (TO BE WRITTEN) ALSO TO debug_data
	assign debug_data = dmemdata;
	////////////////////////////////////////////////////////////
	
	
	// You'll need to change where the dmem and imem read and write...
	dmem mydmem(	.address	(XlatchO[11:0]),
					.clock		(clock),
					//TODO bypass mux output instead of XlatchB
					.data		(dmemdata),
					.wren		(dmemwriteEn),
					.q			(dmemout) // change where output q goes...
	);
	
	imem myimem(	.address 	(pcout[11:0]),
					.clken		(1'b1),
					.clock		(clock),
					.q 			(imemout) // change where output q goes...
	); 
	
endmodule
