module regReadIn(opcode, reginctrl);
input [4:0] opcode;
output reginctrl;
wire [31:0] choice;

decoder de(opcode,choice);


//For Register read B; either Rt or Rd
one_tri(1'b1,choice[0],reginctrl); //If opcode 0, R-Type, need Rt
one_tri(1'b1,choice[5],reginctrl); //addi, need Rt
one_tri(1'b0,choice[7],reginctrl); //sw, need Rd
one_tri(1'b1,choice[8],reginctrl); //lw, doesn't matter since no saving anything
one_tri(1'b0,choice[2],reginctrl); //bne, need RD
one_tri(1'b0,choice[6],reginctrl); //blt, need RD
one_tri(1'b0,choice[1],reginctrl); //j, shouldn't matter
one_tri(1'b0,choice[3],reginctrl); //jal shouldn't matter
one_tri(1'b0,choice[4],reginctrl); //jr need Rd

endmodule 