module regWriteEnable(opcode, regwriteEn, regwritectrl);
input [4:0] opcode;
output regwriteEn,regwritectrl;

wire [31:0] choice;

decoder de(opcode, choice);

//For reg write enable; either 1 or 0, 1 ->write, 0->don't write
one_tri(1'b1,choice[0],regwriteEn); //If opcode 0, R-Type, need 1
one_tri(1'b1,choice[5],regwriteEn);  //addi, need 1
one_tri(1'b0,choice[7],regwriteEn); //Sw, don't write to register
one_tri(1'b1,choice[8],regwriteEn); //Lw, write to register
one_tri(1'b0,choice[2],regwriteEn); //bne, don't write to register
one_tri(1'b0,choice[6],regwriteEn); //blt, don't write to register
one_tri(1'b0,choice[1],regwriteEn); //j, don't write
one_tri(1'b1,choice[3],regwriteEn); //jal, write
one_tri(1'b0,choice[4],regwriteEn); //jr, don't write



//For Register write data; 1->O, 0->D
one_tri(1'b1,choice[0],regwritectrl); //If opcode 0, R-Type, need O
one_tri(1'b1,choice[5],regwritectrl); //addi, need O
one_tri(1'b0,choice[7],regwritectrl); //sw, shouldn't matter since reg write won't be enabled
one_tri(1'b0,choice[8],regwritectrl); //lw, need D from mem
one_tri(1'b1,choice[2],regwritectrl); //bne, shouldn't matter but have O
one_tri(1'b1,choice[6],regwritectrl); //blt, shouldn't matter but have O
one_tri(1'b1,choice[1],regwritectrl); //j, shouldn't matter
one_tri(1'b1,choice[3],regwritectrl); //jal, O
one_tri(1'b1,choice[4],regwritectrl); //jr, shouldn't matter

endmodule 