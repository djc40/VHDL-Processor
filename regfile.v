module regfile(clock, ctrl_writeEnable, ctrl_reset, ctrl_writeReg, ctrl_readRegA, ctrl_readRegB, data_writeReg, data_readRegA, data_readRegB,out22,out23,out24,out25);
   
   input clock, ctrl_writeEnable, ctrl_reset;
   input [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
   input [31:0] data_writeReg;
   output [31:0] data_readRegA, data_readRegB;
   output [31:0] out22, out23, out24, out25;
   
   wire [31:0] m0,m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16,m17,m18,m19,m20,m21,m22,m23,m24,m25,m26,m27,m28,m29,m30,m31;
   wire [31:0] regNum;
   wire [31:0] regControl;
   //TODO: Fix and gate
   ander2 andy(ctrl_writeEnable,regNum,regControl);
   decoder d0(ctrl_writeReg,regNum);
   
   assign out22 = m26;
   assign out23 = m27;
   assign out24 = m28;
   assign out25 = m29;
   
   my_dff dff00(data_writeReg,ctrl_reset, clock, regControl[0], m0); 
   my_dff dff01(data_writeReg,ctrl_reset, clock, regControl[1], m1);
   my_dff dff02(data_writeReg,ctrl_reset, clock, regControl[2], m2);
   my_dff dff03(data_writeReg,ctrl_reset, clock, regControl[3], m3);
   my_dff dff04(data_writeReg,ctrl_reset, clock, regControl[4], m4);
   my_dff dff05(data_writeReg,ctrl_reset, clock, regControl[5], m5);
   my_dff dff06(data_writeReg,ctrl_reset, clock, regControl[6], m6);
   my_dff dff07(data_writeReg,ctrl_reset, clock, regControl[7], m7);
   my_dff dff08(data_writeReg,ctrl_reset, clock, regControl[8], m8);
   my_dff dff09(data_writeReg,ctrl_reset, clock, regControl[9], m9);
   my_dff dff10(data_writeReg,ctrl_reset, clock, regControl[10], m10);
   my_dff dff11(data_writeReg,ctrl_reset, clock, regControl[11], m11);
   my_dff dff12(data_writeReg,ctrl_reset, clock, regControl[12], m12);
   my_dff dff13(data_writeReg,ctrl_reset, clock, regControl[13], m13);
   my_dff dff14(data_writeReg,ctrl_reset, clock, regControl[14], m14);
   my_dff dff15(data_writeReg,ctrl_reset, clock, regControl[15], m15);
   my_dff dff16(data_writeReg,ctrl_reset, clock, regControl[16], m16);
   my_dff dff17(data_writeReg,ctrl_reset, clock, regControl[17], m17);
   my_dff dff18(data_writeReg,ctrl_reset, clock, regControl[18], m18);
   my_dff dff19(data_writeReg,ctrl_reset, clock, regControl[19], m19);
   my_dff dff20(data_writeReg,ctrl_reset, clock, regControl[20], m20);
   my_dff dff21(data_writeReg,ctrl_reset, clock, regControl[21], m21);
   my_dff dff22(data_writeReg,ctrl_reset, clock, regControl[22], m22);
   my_dff dff23(data_writeReg,ctrl_reset, clock, regControl[23], m23);
   my_dff dff24(data_writeReg,ctrl_reset, clock, regControl[24], m24);
   my_dff dff25(data_writeReg,ctrl_reset, clock, regControl[25], m25);
   my_dff dff26(data_writeReg,ctrl_reset, clock, regControl[26], m26);
   my_dff dff27(data_writeReg,ctrl_reset, clock, regControl[27], m27);
   my_dff dff28(data_writeReg,ctrl_reset, clock, regControl[28], m28);
   my_dff dff29(data_writeReg,ctrl_reset, clock, regControl[29], m29);
   my_dff dff30(data_writeReg,ctrl_reset, clock, regControl[30], m30);
   my_dff dff31(data_writeReg,ctrl_reset, clock, regControl[31], m31);
   
   
   mulitplex mulA(m0,m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16,m17,m18,m19,m20,m21,m22,m23,m24,m25,m26,m27,m28,m29,m30,m31,ctrl_readRegA,data_readRegA);
   mulitplex mulB(m0,m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16,m17,m18,m19,m20,m21,m22,m23,m24,m25,m26,m27,m28,m29,m30,m31,ctrl_readRegB,data_readRegB);
   
   
   
   
     
   
endmodule

