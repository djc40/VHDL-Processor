module rlayer4(d,q,choice);

input [31:0] d;
input choice;
output [31:0] q;


multi m0(d[0],d[16],q[0],choice);
multi m1(d[1],d[17],q[1],choice);
multi m2(d[2],d[18],q[2],choice);
multi m3(d[3],d[19],q[3],choice);
multi m4(d[4],d[20],q[4],choice);
multi m5(d[5],d[21],q[5],choice);
multi m6(d[6],d[22],q[6],choice);
multi m7(d[7],d[23],q[7],choice);
multi m8(d[8],d[24],q[8],choice);
multi m9(d[9],d[25],q[9],choice);
multi m10(d[10],d[26],q[10],choice);
multi m11(d[11],d[27],q[11],choice);
multi m12(d[12],d[28],q[12],choice);
multi m13(d[13],d[29],q[13],choice);
multi m14(d[14],d[30],q[14],choice);
multi m15(d[15],d[31],q[15],choice);
multi m16(d[16],d[31],q[16],choice);
multi m17(d[17],d[31],q[17],choice);
multi m18(d[18],d[31],q[18],choice);
multi m19(d[19],d[31],q[19],choice);
multi m20(d[20],d[31],q[20],choice);
multi m21(d[21],d[31],q[21],choice);
multi m22(d[22],d[31],q[22],choice);
multi m23(d[23],d[31],q[23],choice);
multi m24(d[24],d[31],q[24],choice);
multi m25(d[25],d[31],q[25],choice);
multi m26(d[26],d[31],q[26],choice);
multi m27(d[27],d[31],q[27],choice);
multi m28(d[28],d[31],q[28],choice);
multi m29(d[29],d[31],q[29],choice);
multi m30(d[30],d[31],q[30],choice);
multi m31(d[31],d[31],q[31],choice);

endmodule
