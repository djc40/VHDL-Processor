module skeleton(inclock, resetn, ps2_clock, ps2_data, debug_word, debug_addr, leds, 
					 lcd_data, lcd_rw, lcd_en, lcd_rs, lcd_on, lcd_blon, 	
					 seg1, seg2, seg3, seg4, seg5, seg6, seg7, seg8,but1,but2,but3,but4,
					 vga_clock, vga_hs_sync, vga_vs_sync, vga_blank, vga_sync,
					 R, G, B);

	input 				inclock, resetn;
	input					but1, but2, but3, but4;
	inout 				ps2_data, ps2_clock;
	
	output 				lcd_rw, lcd_en, lcd_rs, lcd_on, lcd_blon;
	output 	[7:0] 	leds, lcd_data;
	output 	[6:0] 	seg1, seg2, seg3, seg4, seg5, seg6, seg7, seg8;
	output 	[31:0] 	debug_word;
	output   [11:0]  	debug_addr;
	
	
	//VGAModule
	output vga_clock, vga_hs_sync, vga_vs_sync, vga_blank, vga_sync;
	output	[7:0] 	R, G, B;
	
	wire					clock;
	wire					lcd_write_en;
	wire 		[31:0]	lcd_write_data,paddle1,paddle2,ballx,bally;
	wire		[7:0]		ps2_key_data;
	wire					ps2_key_pressed;
	wire		[7:0]		ps2_out;	

	
	
	//module vga(FPGA_clock, vga_clock, R, G, B, vga_hs_sync, vga_vs_sync, vga_blank, vga_sync, plY, prY, bX, bY);
   vga vgaModule(inclock, vga_clock, R, G, B, vga_hs_sync, vga_vs_sync, vga_blank, vga_sync, paddle1, paddle2, ballx, bally);
	
	
	// clock divider (by 5, i.e., 10 MHz)
	pll div(inclock,clock);
	
	// UNCOMMENT FOLLOWING LINE AND COMMENT ABOVE LINE TO RUN AT 50 MHz
	//assign clock = inclock;
	
	// your processor
	processor myprocessor(clock, ~resetn, ps2_key_pressed, ps2_out, lcd_write_en, lcd_write_data, debug_word, debug_addr,but1,but2,but3,but4,paddle1,paddle2,ballx,bally);
	
	// keyboard controller
	PS2_Interface myps2(clock, resetn, ps2_clock, ps2_data, ps2_key_data, ps2_key_pressed, ps2_out);
	
	// lcd controller
	lcd mylcd(clock, ~resetn, lcd_write_en, lcd_write_data[7:0], lcd_data, lcd_rw, lcd_en, lcd_rs, lcd_on, lcd_blon);
	
	// example for sending ps2 data to the first two seven segment displays
	Hexadecimal_To_Seven_Segment hex1(ps2_out[3:0], seg1);
	Hexadecimal_To_Seven_Segment hex2(ps2_out[7:4], seg2);
	
	// the other seven segment displays are currently set to 0
	Hexadecimal_To_Seven_Segment hex3(4'b0, seg3);
	Hexadecimal_To_Seven_Segment hex4(4'b0, seg4);
	Hexadecimal_To_Seven_Segment hex5(4'b0, seg5);
	Hexadecimal_To_Seven_Segment hex6(4'b0, seg6);
	Hexadecimal_To_Seven_Segment hex7(4'b0, seg7);
	Hexadecimal_To_Seven_Segment hex8(4'b0, seg8);
	
	// some LEDs that you could use for debugging if you wanted
	assign leds[0] = ~but1;
	assign leds[1] = ~but2;
	assign leds[2] = ~but3;
	assign leds[3] = ~but4;
	assign leds[7:4] = 4'b0000;
	
	//assign leds = 8'b00101011;
	
endmodule