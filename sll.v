module sll(d,q,choice);

input [31:0] d;
input[4:0] choice;
output [31:0] q;
wire [31:0] w0,w1,w2,w3;



layer  l0(d,w0,choice[0]);
layer1 l1(w0,w1,choice[1]);
layer2 l2(w1,w2,choice[2]);
layer3 l3(w2,w3,choice[3]);
layer4 l4(w3,q,choice[4]);

endmodule