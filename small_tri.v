module small_tri(in, oe, out);

	input [4:0] in;
	input oe;
	
	output [4:0] out;

	assign out = oe ? in :1'bz;
endmodule 