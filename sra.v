module sra(d,q,choice);

input [31:0] d;
input[4:0] choice;
output [31:0] q;
wire [31:0] w0,w1,w2,w3;

rlayer  lr0(d,w0,choice[0]);
rlayer1 r1(w0,w1,choice[1]);
rlayer2 r2(w1,w2,choice[2]);
rlayer3 r3(w2,w3,choice[3]);
rlayer4 r4(w3,q,choice[4]);

endmodule