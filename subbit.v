module subbit(code,c);

input [4:0] code;
output c;

assign c = code[0]&~code[1]&~code[2]&~code[3]&~code[4];

endmodule