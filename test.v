	module test(clock,FlatchPC,regoutA,regoutB,FlatchIR,ext,mbout,aluout,latchDpc,latchDir,latchDA,latchDB);
	input clock;
	input [31:0] FlatchPC, regoutA, regoutB, FlatchIR;
	output [31:0] ext,mbout,aluout,latchDpc,latchDir,latchDA,latchDB;
	wire [31:0] DlatchPC,DlatchA,DlatchB,DlatchIR;
	
	DLatcha latch1(clock,FlatchPC,regoutA,regoutB,FlatchIR,DlatchPC,DlatchA,DlatchB,DlatchIR);
	
	assign latchDpc = DlatchPC;
	assign latchDir = DlatchIR;
	assign latchDA = DlatchA;
	assign latchDB = DlatchB;
	//*DLatch
	
	//Execute
	wire nE,lT;
	wire [31:0] sxi, newpc, muxB, result;
	wire [4:0] alucode, muxbit;
	//sign extender
	extender extend(DlatchIR[16:0],sxi);
	assign ext = sxi;
	my_tri muxer2(DlatchB,muxbit[0],muxB);
	my_tri muxer3(sxi,muxbit[1],muxB);
	assign mbout = muxbit;
	
	ALUController(DlatchIR[31:27],DlatchIR[6:2],alucode,muxbit);
	
	ALU myalu1(DlatchPC, sxi, 5'b00000, 5'b00000, newpc,,);
	ALU myalu2(DlatchA, muxB, alucode, DlatchIR[11:7], result, nE, lT);
	//*Execute
	assign aluout = result;
	//XLatch
	wire [31:1] Xlatcha, Xlatchd, XlatchIR;
	XLatch latch2(clock,result,DlatchB,DlatchIR,Xlatcha,Xlatchd,XlatchIR);
	
	endmodule 
