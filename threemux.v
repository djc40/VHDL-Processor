module threemux(in1, in2, in3, ctrl, out);
input [1:0] ctrl;
input [31:0] in1, in2, in3;
output [31:0] out;

wire [2:0] choice;

assign choice[0] =  ~ ctrl[1] & ~ctrl[0];
assign choice[1] =  ~ctrl[1] & ctrl[0];
assign choice[2] = ctrl[1] & ~ctrl[0];

// my_tri(in, oe, out)
my_tri(in3,choice[2],out); //on 2 output
my_tri(in2,choice[1],out); //on 1 output
my_tri(in1,choice[0],out); //on 0 output


endmodule