module threemuxpc(in1, in2, in3, isbranch,isjump, out);
input isbranch, isjump;
input [31:0] in1, in2, in3;
output [31:0] out;

wire [2:0] choice;

assign choice[0] =  ~ isbranch & ~isjump;
assign choice[1] =  isjump;
assign choice[2] = isbranch;

// my_tri(in, oe, out)
my_tri(in3,choice[2],out); //on 2 output
my_tri(in2,choice[1],out); //on 1 output
my_tri(in1,choice[0],out); //on 0 output


endmodule