module two_tri(in, oe, out);

	input [1:0] in;
	input oe;
	
	output [1:0] out;

	assign out = oe ? in :1'bz;
endmodule 