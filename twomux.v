module twomux(in1, in2, ctrl, out);
input ctrl;
input [31:0] in1, in2;
output [31:0] out;

// my_tri(in, oe, out)

my_tri B(in1,ctrl,out); //on 1 output
my_tri Sx(in2,~ctrl,out); //on 0 output


endmodule