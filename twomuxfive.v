module twomuxfive(in1, in2, ctrl, out);
input ctrl;
input [4:0] in1, in2;
output [4:0] out;

// my_tri(in, oe, out)

small_tri B(in1,ctrl,out); //on 1 output
small_tri Sx(in2,~ctrl,out); //on 0 output


endmodule